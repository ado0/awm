--[[

     Licensed under GNU General Public License v2
      * (c) 2013,      Luke Bonham
      * (c) 2010-2012, Peter Hofmann
      * (c) 2017,      Ado

--]]

local helpers  = require("lain.helpers")
local wibox    = require("wibox")
local math     = { ceil   = math.ceil }
local string   = { format = string.format,
                   gmatch = string.gmatch }
local tostring = tostring

-- CPU curr freq usage
-- lain.widget.freq

local function factory(args)
    local freq      = { widget = wibox.widget.textbox() }
    local args     = args or {}
    local tempfile = "/sys/devices/system/cpu/cpufreq/policy0/scaling_cur_freq"
    local timeout  = args.timeout or 2
    local settings = args.settings or function() end

    function freq.update()
        local freq_curr = helpers.first_line(tempfile)
	local freq_mhz  = (freq_curr - freq_curr % 1000) / 1000;
	local freq_ghz  = (freq_mhz  - freq_mhz  % 100 ) / 1000;

	freq_disp = tostring(freq_ghz)
	widget = freq.widget
        settings()
    end

    helpers.newtimer("freq", timeout, freq.update)

    return freq
end

return factory
